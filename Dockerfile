# A little fat but whatever
FROM ubuntu:focal

USER root

SHELL ["/bin/bash", "-l", "-c"]

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Berlin

# Install packages
RUN dpkg --add-architecture i386 \
		&& apt-get update \
		&& apt-get install -y \
				wine \
				wine32 \
				libgdbm-dev \
				libncurses5-dev \
				automake \
				libtool \
				bison \
				libffi-dev \
                python-is-python3 \
                binutils \
                build-essential \
				bison \
                mono-runtime \
				mingw-w64 \
				cmake \
				g++-multilib \
				libc6-dev-i386 \
				curl \
				zstd \
				tree \
				openssl \
				unzip \
				git \
		&& rm -rf /var/lib/apt/lists/*

# Install ruby, hopefully
RUN curl -sSL https://rvm.io/mpapis.asc | gpg --import -
RUN curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
RUN curl -sSL https://get.rvm.io | bash -s -- --version "1.29.12"
RUN usermod -a -G rvm root
USER root
RUN /bin/bash -c "source /etc/profile.d/rvm.sh"
RUN /bin/bash -c "source /usr/local/rvm/scripts/rvm"
RUN rvm requirements
RUN rvm install ruby-3.0
RUN rvm use 3.0 --default
RUN /bin/bash -c "source $(rvm 3.0 do rvm env --path)"
RUN ruby -v

# Install random gems
RUN gem install rake binding_of_caller chunky_png digest-xxhash xml-mapping
RUN /bin/bash -c "source $(rvm 3.0 do rvm env --path)"
RUN rake --version

# Install flexspin, hopefully
RUN git clone https://github.com/totalspectrum/spin2cpp
RUN (cd spin2cpp && git checkout 183cd6c67ff8a417c4d3f0021514af9adc78f4e0 && make && chmod -R +x+r *)
ENV PATH="${PATH}:/spin2cpp/build"
RUN pwd
RUN echo $PATH
RUN flexspin --version


ENV HOME /root
ENV WINEPREFIX /root/.wine
ENV WINEARCH win32

#COPY mingw-get /root/mingw-get/

#RUN wine /root/mingw-get/bin/mingw-get.exe install mingw32-base-bin mingw32-gcc-g++-bin msys-base-bin
